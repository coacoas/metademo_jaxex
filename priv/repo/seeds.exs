# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     MetademoJaxex.Repo.insert!(%MetademoJaxex.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias MetademoJaxex.Repo
alias MetademoJaxex.Metademo.HostRole
alias MetademoJaxex.Metademo.HostType
alias MetademoJaxex.Metademo.Datacenter
alias MetademoJaxex.Metademo.Host
alias MetademoJaxex.Metademo.Pod
alias MetademoJaxex.Metademo.PodMember

# HOST ROLES
hr_websrv =
  Repo.insert!(%HostRole{
    name: "Web Server",
    description: "use for hosts that are a web server",
    is_active: 1,
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

hr_appsrv =
  Repo.insert!(%HostRole{
    name: "App Server",
    description: "use for hosts that are an app server",
    is_active: 1,
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

hr_dbsrv =
  Repo.insert!(%HostRole{
    name: "Database Server",
    description: "use for hosts that are a database server",
    is_active: 1,
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

hr_hyper =
  Repo.insert!(%HostRole{
    name: "VM Host",
    description: "use for hosting Virtual Machinges",
    is_active: 1,
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

# HOST TYPES
ht_virt =
  Repo.insert!(%HostType{
    name: "VIRTUAL",
    description: "use this type for virtual machines type hosts",
    is_active: 1,
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

ht_bare =
  Repo.insert!(%HostType{
    name: "BAREMETAL",
    description: "use this type if host is baremetal",
    is_active: 1,
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

ht_hyper =
  Repo.insert!(%HostType{
    name: "HYPERVISOR",
    description: "use this type for hosts that will have virtual machines",
    is_active: 1,
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

# DATACENTERS
dc_austin =
  Repo.insert!(%Datacenter{
    code: "AUS",
    name: "Austin",
    is_active: 1,
    pattern: "",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

dc_hongkg =
  Repo.insert!(%Datacenter{
    code: "HK",
    name: "Hong Kong",
    is_active: 1,
    pattern: "",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

dc_miami =
  Repo.insert!(%Datacenter{
    code: "MIA",
    name: "Miami",
    is_active: 1,
    pattern: "",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

# HOSTS
host_one =
  Repo.insert!(%Host{
    hostname: "web.somehost01.com",
    host_role_id: hr_websrv.id,
    host_type_id: ht_virt.id,
    datacenter_id: dc_austin.id,
    is_active: 1,
    uuid: "ABCD-EFGH-IJKL",
    ip_address: "192.168.1.4",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

host_two =
  Repo.insert!(%Host{
    hostname: "app.somehost02.com",
    host_role_id: hr_appsrv.id,
    host_type_id: ht_virt.id,
    datacenter_id: dc_hongkg.id,
    is_active: 1,
    uuid: "EFGH-IJKL-ABCD",
    ip_address: "192.168.3.4",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

host_tre =
  Repo.insert!(%Host{
    hostname: "db.somehost03.com",
    host_role_id: hr_dbsrv.id,
    host_type_id: ht_bare.id,
    datacenter_id: dc_miami.id,
    is_active: 1,
    uuid: "IJKL-ABCD-EFGH",
    ip_address: "192.168.2.4",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

_host_dom0 =
  Repo.insert!(%Host{
    hostname: "dom0.somehost04.com",
    host_role_id: hr_hyper.id,
    host_type_id: ht_hyper.id,
    datacenter_id: dc_miami.id,
    is_active: 1,
    uuid: "MNOP-ABCD-EFGH",
    ip_address: "192.168.4.1",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

# PODS
pod_one =
  Repo.insert!(%Pod{
    name: "PodOne",
    physical_podname: "PodOne-DG1",
    is_active: 1,
    type: "Basion",
    zone: "MW",
    datacenter_id: dc_austin.id,
    port: "1532",
    sid: "bobots",
    db_host: "dbsrv01",
    db_host2: "dbsrv02",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

pod_two =
  Repo.insert!(%Pod{
    name: "PodTwo",
    physical_podname: "PodTwo-AG1",
    is_active: 1,
    type: "Zurl",
    zone: "SE",
    datacenter_id: dc_miami.id,
    port: "1532",
    sid: "nurny",
    db_host: "dbsrv03",
    db_host2: "dbsrv04",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

# POD MEMBERS
Repo.insert!(%PodMember{
  pod_id: pod_one.id,
  host_id: host_one.id,
  is_active: 1,
  inserted_by: "Seeder",
  updated_by: "Seeder"
})

Repo.insert!(%PodMember{
  pod_id: pod_one.id,
  host_id: host_two.id,
  is_active: 1,
  inserted_by: "Seeder",
  updated_by: "Seeder"
})

Repo.insert!(%PodMember{
  pod_id: pod_two.id,
  host_id: host_tre.id,
  is_active: 1,
  inserted_by: "Seeder",
  updated_by: "Seeder"
})

# HOST ATTR
_ha_one_one =
  Ecto.build_assoc(host_one, :host_attrs, %{
    name: "metademo_version",
    value: "1.1.1.",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

_ha_one_two =
  Ecto.build_assoc(host_one, :host_attrs, %{
    name: "os_version",
    value: "4.15.0-58-generic #64-Ubuntu",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

_ha_two_one =
  Ecto.build_assoc(host_two, :host_attrs, %{
    name: "metademo_version",
    value: "1.1.1.",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

_ha_two_two =
  Ecto.build_assoc(host_two, :host_attrs, %{
    name: "os_version",
    value: "3.1.0-58-generic #64-Centos",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })

_ha_tre_one =
  Ecto.build_assoc(host_tre, :host_attrs, %{
    name: "os_version",
    value: "4.20.0-69-generic #64-Manjaro",
    inserted_by: "Seeder",
    updated_by: "Seeder"
  })
