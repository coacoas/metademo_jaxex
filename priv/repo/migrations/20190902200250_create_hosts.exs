defmodule MetademoJaxex.Repo.Migrations.CreateHosts do
  use Ecto.Migration

  def change do
    create table(:hosts) do
      add :hostname, :string, null: false
      add :is_active, :integer, null: false, default: 1
      add :inserted_by, :string, null: false
      add :updated_by, :string, null: false
      add :uuid, :string, null: false
      add :ip_address, :string, null: false
      add :host_role_id, references(:host_roles, on_delete: :nothing), null: false
      add :host_type_id, references(:host_types, on_delete: :nothing), null: false
      add :datacenter_id, references(:datacenters, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:hosts, [:host_role_id])
    create index(:hosts, [:host_type_id])
    create index(:hosts, [:datacenter_id])
    create unique_index(:hosts, [:hostname])
    create unique_index(:hosts, [:uuid])
  end
end
