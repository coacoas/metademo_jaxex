defmodule MetademoJaxexWeb.PodMemberView do
  use MetademoJaxexWeb, :view
  alias MetademoJaxexWeb.PodMemberView

  def render("index.json", %{pod_members: pod_members}) do
    %{data: render_many(pod_members, PodMemberView, "pod_member.json")}
  end

  def render("show.json", %{pod_member: pod_member}) do
    %{data: render_one(pod_member, PodMemberView, "pod_member.json")}
  end

  def render("pod_member.json", %{pod_member: pod_member}) do
    %{
      id: pod_member.id,
      pod_id: pod_member.pod_id,
      host_id: pod_member.host_id,
      is_active: pod_member.is_active,
      inserted_by: pod_member.inserted_by,
      updated_by: pod_member.updated_by
    }
  end
end
