defmodule MetademoJaxexWeb.DatacenterController do
  use MetademoJaxexWeb, :controller

  alias MetademoJaxex.Metademo
  alias MetademoJaxex.Metademo.Datacenter

  action_fallback MetademoJaxexWeb.FallbackController

  def index(conn, _params) do
    datacenters = Metademo.list_datacenters()
    render(conn, "index.json", datacenters: datacenters)
  end

  def create(conn, %{"datacenter" => datacenter_params}) do
    with {:ok, %Datacenter{} = datacenter} <- Metademo.create_datacenter(datacenter_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.datacenter_path(conn, :show, datacenter))
      |> render("show.json", datacenter: datacenter)
    end
  end

  def show(conn, %{"id" => id}) do
    datacenter = Metademo.get_datacenter!(id)
    render(conn, "show.json", datacenter: datacenter)
  end

  def update(conn, %{"id" => id, "datacenter" => datacenter_params}) do
    datacenter = Metademo.get_datacenter!(id)

    with {:ok, %Datacenter{} = datacenter} <-
           Metademo.update_datacenter(datacenter, datacenter_params) do
      render(conn, "show.json", datacenter: datacenter)
    end
  end

  def delete(conn, %{"id" => id}) do
    datacenter = Metademo.get_datacenter!(id)

    with {:ok, %Datacenter{}} <- Metademo.delete_datacenter(datacenter) do
      send_resp(conn, :no_content, "")
    end
  end
end
