# defmodule MetademoJaxexWeb.PodMemberController do
#   use MetademoJaxexWeb, :controller

#   alias MetademoJaxex.Metademo
#   alias MetademoJaxex.Metademo.PodMember

#   action_fallback MetademoJaxexWeb.FallbackController

#   def index(conn, _params) do
#     pod_members = Metademo.list_pod_members()
#     render(conn, "index.json", pod_members: pod_members)
#   end

#   def create(conn, %{"pod_member" => pod_member_params}) do
#     with {:ok, %PodMember{} = pod_member} <- Metademo.create_pod_member(pod_member_params) do
#       conn
#       |> put_status(:created)
#       |> put_resp_header("location", Routes.pod_member_path(conn, :show, pod_member))
#       |> render("show.json", pod_member: pod_member)
#     end
#   end

#   def show(conn, %{"id" => id}) do
#     pod_member = Metademo.get_pod_member!(id)
#     render(conn, "show.json", pod_member: pod_member)
#   end

#   def update(conn, %{"id" => id, "pod_member" => pod_member_params}) do
#     pod_member = Metademo.get_pod_member!(id)

#     with {:ok, %PodMember{} = pod_member} <- Metademo.update_pod_member(pod_member, pod_member_params) do
#       render(conn, "show.json", pod_member: pod_member)
#     end
#   end

#   def delete(conn, %{"id" => id}) do
#     pod_member = Metademo.get_pod_member!(id)

#     with {:ok, %PodMember{}} <- Metademo.delete_pod_member(pod_member) do
#       send_resp(conn, :no_content, "")
#     end
#   end
# end
