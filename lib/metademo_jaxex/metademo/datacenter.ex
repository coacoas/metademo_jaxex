defmodule MetademoJaxex.Metademo.Datacenter do
  use Ecto.Schema
  import Ecto.Changeset

  schema "datacenters" do
    field :code, :string
    field :inserted_by, :string
    field :is_active, :integer
    field :name, :string
    field :pattern, :string
    field :updated_by, :string

    timestamps()
  end

  @doc false
  def changeset(datacenter, attrs) do
    datacenter
    |> cast(attrs, [:code, :name, :pattern, :inserted_by, :updated_by, :is_active])
    |> validate_required([:code, :name, :inserted_by, :updated_by, :is_active])
  end
end
