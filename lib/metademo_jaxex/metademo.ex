defmodule MetademoJaxex.Metademo do
  @moduledoc """
  The Metademo context.
  """

  import Ecto.Query, warn: false
  alias MetademoJaxex.Repo

  alias MetademoJaxex.Metademo.HostRole

  @doc """
  Returns the list of host_roles.

  ## Examples

      iex> list_host_roles()
      [%HostRole{}, ...]

  """
  def list_host_roles do
    Repo.all(HostRole)
  end

  @doc """
  Gets a single host_role.

  Raises `Ecto.NoResultsError` if the Host role does not exist.

  ## Examples

      iex> get_host_role!(123)
      %HostRole{}

      iex> get_host_role!(456)
      ** (Ecto.NoResultsError)

  """
  def get_host_role!(id), do: Repo.get!(HostRole, id)

  @doc """
  Creates a host_role.

  ## Examples

      iex> create_host_role(%{field: value})
      {:ok, %HostRole{}}

      iex> create_host_role(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_host_role(attrs \\ %{}) do
    %HostRole{}
    |> HostRole.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a host_role.

  ## Examples

      iex> update_host_role(host_role, %{field: new_value})
      {:ok, %HostRole{}}

      iex> update_host_role(host_role, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_host_role(%HostRole{} = host_role, attrs) do
    host_role
    |> HostRole.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a HostRole.

  ## Examples

      iex> delete_host_role(host_role)
      {:ok, %HostRole{}}

      iex> delete_host_role(host_role)
      {:error, %Ecto.Changeset{}}

  """
  def delete_host_role(%HostRole{} = host_role) do
    Repo.delete(host_role)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking host_role changes.

  ## Examples

      iex> change_host_role(host_role)
      %Ecto.Changeset{source: %HostRole{}}

  """
  def change_host_role(%HostRole{} = host_role) do
    HostRole.changeset(host_role, %{})
  end

  alias MetademoJaxex.Metademo.HostType

  @doc """
  Returns the list of host_types.

  ## Examples

      iex> list_host_types()
      [%HostType{}, ...]

  """
  def list_host_types do
    Repo.all(HostType)
  end

  @doc """
  Gets a single host_type.

  Raises `Ecto.NoResultsError` if the Host type does not exist.

  ## Examples

      iex> get_host_type!(123)
      %HostType{}

      iex> get_host_type!(456)
      ** (Ecto.NoResultsError)

  """
  def get_host_type!(id), do: Repo.get!(HostType, id)

  @doc """
  Creates a host_type.

  ## Examples

      iex> create_host_type(%{field: value})
      {:ok, %HostType{}}

      iex> create_host_type(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_host_type(attrs \\ %{}) do
    %HostType{}
    |> HostType.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a host_type.

  ## Examples

      iex> update_host_type(host_type, %{field: new_value})
      {:ok, %HostType{}}

      iex> update_host_type(host_type, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_host_type(%HostType{} = host_type, attrs) do
    host_type
    |> HostType.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a HostType.

  ## Examples

      iex> delete_host_type(host_type)
      {:ok, %HostType{}}

      iex> delete_host_type(host_type)
      {:error, %Ecto.Changeset{}}

  """
  def delete_host_type(%HostType{} = host_type) do
    Repo.delete(host_type)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking host_type changes.

  ## Examples

      iex> change_host_type(host_type)
      %Ecto.Changeset{source: %HostType{}}

  """
  def change_host_type(%HostType{} = host_type) do
    HostType.changeset(host_type, %{})
  end

  alias MetademoJaxex.Metademo.Datacenter

  @doc """
  Returns the list of datacenters.

  ## Examples

      iex> list_datacenters()
      [%Datacenter{}, ...]

  """
  def list_datacenters do
    Repo.all(Datacenter)
  end

  @doc """
  Gets a single datacenter.

  Raises `Ecto.NoResultsError` if the Datacenter does not exist.

  ## Examples

      iex> get_datacenter!(123)
      %Datacenter{}

      iex> get_datacenter!(456)
      ** (Ecto.NoResultsError)

  """
  def get_datacenter!(id), do: Repo.get!(Datacenter, id)

  @doc """
  Creates a datacenter.

  ## Examples

      iex> create_datacenter(%{field: value})
      {:ok, %Datacenter{}}

      iex> create_datacenter(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_datacenter(attrs \\ %{}) do
    %Datacenter{}
    |> Datacenter.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a datacenter.

  ## Examples

      iex> update_datacenter(datacenter, %{field: new_value})
      {:ok, %Datacenter{}}

      iex> update_datacenter(datacenter, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_datacenter(%Datacenter{} = datacenter, attrs) do
    datacenter
    |> Datacenter.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Datacenter.

  ## Examples

      iex> delete_datacenter(datacenter)
      {:ok, %Datacenter{}}

      iex> delete_datacenter(datacenter)
      {:error, %Ecto.Changeset{}}

  """
  def delete_datacenter(%Datacenter{} = datacenter) do
    Repo.delete(datacenter)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking datacenter changes.

  ## Examples

      iex> change_datacenter(datacenter)
      %Ecto.Changeset{source: %Datacenter{}}

  """
  def change_datacenter(%Datacenter{} = datacenter) do
    Datacenter.changeset(datacenter, %{})
  end

  alias MetademoJaxex.Metademo.Host

  @doc """
  Returns the list of hosts.

  ## Examples

      iex> list_hosts()
      [%Host{}, ...]

  """
  def list_hosts do
    Repo.all(Host)
  end

  @doc """
  Gets a single host.

  Raises `Ecto.NoResultsError` if the Host does not exist.

  ## Examples

      iex> get_host!(123)
      %Host{}

      iex> get_host!(456)
      ** (Ecto.NoResultsError)

  """
  def get_host!(id), do: Repo.get!(Host, id)

  @doc """
  Creates a host.

  ## Examples

      iex> create_host(%{field: value})
      {:ok, %Host{}}

      iex> create_host(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_host(attrs \\ %{}) do
    %Host{}
    |> Host.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a host.

  ## Examples

      iex> update_host(host, %{field: new_value})
      {:ok, %Host{}}

      iex> update_host(host, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_host(%Host{} = host, attrs) do
    host
    |> Host.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Host.

  ## Examples

      iex> delete_host(host)
      {:ok, %Host{}}

      iex> delete_host(host)
      {:error, %Ecto.Changeset{}}

  """
  def delete_host(%Host{} = host) do
    Repo.delete(host)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking host changes.

  ## Examples

      iex> change_host(host)
      %Ecto.Changeset{source: %Host{}}

  """
  def change_host(%Host{} = host) do
    Host.changeset(host, %{})
  end

  alias MetademoJaxex.Metademo.HostAttr

  @doc """
  Returns the list of host_attrs.

  ## Examples

      iex> list_host_attrs()
      [%HostAttr{}, ...]

  """
  def list_host_attrs do
    Repo.all(HostAttr)
  end

  @doc """
  Gets a single host_attr.

  Raises `Ecto.NoResultsError` if the Host attr does not exist.

  ## Examples

      iex> get_host_attr!(123)
      %HostAttr{}

      iex> get_host_attr!(456)
      ** (Ecto.NoResultsError)

  """
  def get_host_attr!(id), do: Repo.get!(HostAttr, id)

  @doc """
  Creates a host_attr.

  ## Examples

      iex> create_host_attr(%{field: value})
      {:ok, %HostAttr{}}

      iex> create_host_attr(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """

  # ORIGINAL
  # def create_host_attr(attrs \\ %{}) do
  #   %HostAttr{}
  #   |> HostAttr.changeset(attrs)
  #   |> Repo.insert()
  # end

  # UPDATE: making it work :)
  def create_host_attr(attrs \\ %{}, host) do
    %HostAttr{}
    |> Map.put(:host_id, host.id)
    |> HostAttr.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a host_attr.

  ## Examples

      iex> update_host_attr(host_attr, %{field: new_value})
      {:ok, %HostAttr{}}

      iex> update_host_attr(host_attr, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_host_attr(%HostAttr{} = host_attr, attrs) do
    host_attr
    |> HostAttr.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a HostAttr.

  ## Examples

      iex> delete_host_attr(host_attr)
      {:ok, %HostAttr{}}

      iex> delete_host_attr(host_attr)
      {:error, %Ecto.Changeset{}}

  """
  def delete_host_attr(%HostAttr{} = host_attr) do
    Repo.delete(host_attr)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking host_attr changes.

  ## Examples

      iex> change_host_attr(host_attr)
      %Ecto.Changeset{source: %HostAttr{}}

  """
  def change_host_attr(%HostAttr{} = host_attr) do
    HostAttr.changeset(host_attr, %{})
  end

  alias MetademoJaxex.Metademo.Pod

  @doc """
  Returns the list of pod.

  ## Examples

      iex> list_pod()
      [%Pod{}, ...]

  """
  def list_pod do
    Repo.all(Pod)
  end

  @doc """
  Gets a single pod.

  Raises `Ecto.NoResultsError` if the Pod does not exist.

  ## Examples

      iex> get_pod!(123)
      %Pod{}

      iex> get_pod!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pod!(id), do: Repo.get!(Pod, id)

  @doc """
  Creates a pod.

  ## Examples

      iex> create_pod(%{field: value})
      {:ok, %Pod{}}

      iex> create_pod(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pod(attrs \\ %{}) do
    %Pod{}
    |> Pod.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a pod.

  ## Examples

      iex> update_pod(pod, %{field: new_value})
      {:ok, %Pod{}}

      iex> update_pod(pod, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pod(%Pod{} = pod, attrs) do
    pod
    |> Pod.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Pod.

  ## Examples

      iex> delete_pod(pod)
      {:ok, %Pod{}}

      iex> delete_pod(pod)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pod(%Pod{} = pod) do
    Repo.delete(pod)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pod changes.

  ## Examples

      iex> change_pod(pod)
      %Ecto.Changeset{source: %Pod{}}

  """
  def change_pod(%Pod{} = pod) do
    Pod.changeset(pod, %{})
  end

  alias MetademoJaxex.Metademo.PodMember

  @doc """
  Returns the list of pod_members.

  ## Examples

      iex> list_pod_members()
      [%PodMember{}, ...]

  """
  def list_pod_members do
    Repo.all(PodMember)
  end

  @doc """
  Gets a single pod_member.

  Raises `Ecto.NoResultsError` if the Pod member does not exist.

  ## Examples

      iex> get_pod_member!(123)
      %PodMember{}

      iex> get_pod_member!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pod_member!(id), do: Repo.get!(PodMember, id)

  @doc """
  Creates a pod_member.

  ## Examples

      iex> create_pod_member(%{field: value})
      {:ok, %PodMember{}}

      iex> create_pod_member(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pod_member(attrs \\ %{}) do
    %PodMember{}
    |> PodMember.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a pod_member.

  ## Examples

      iex> update_pod_member(pod_member, %{field: new_value})
      {:ok, %PodMember{}}

      iex> update_pod_member(pod_member, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pod_member(%PodMember{} = pod_member, attrs) do
    pod_member
    |> PodMember.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a PodMember.

  ## Examples

      iex> delete_pod_member(pod_member)
      {:ok, %PodMember{}}

      iex> delete_pod_member(pod_member)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pod_member(%PodMember{} = pod_member) do
    Repo.delete(pod_member)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pod_member changes.

  ## Examples

      iex> change_pod_member(pod_member)
      %Ecto.Changeset{source: %PodMember{}}

  """
  def change_pod_member(%PodMember{} = pod_member) do
    PodMember.changeset(pod_member, %{})
  end
end
