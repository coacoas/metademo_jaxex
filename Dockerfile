FROM elixir:1.7.4
MAINTAINER John F. Hogarty <hogihung@gmail.com>

RUN apt-get update -qq && apt-get install -y postgresql-client netcat && \
    mix local.hex --force && \
    mix local.rebar --force

RUN mkdir -p /metademo-jaxex
WORKDIR /metademo-jaxex

COPY . .

RUN elixir -v

RUN mix deps.clean --all && \
    mix deps.get && \
    mix compile --warnings-as-errors

EXPOSE 4000
EXPOSE 4001

ENTRYPOINT ["sh", "./.docker/startup.sh"]
