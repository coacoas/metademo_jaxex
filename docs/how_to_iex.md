# README

This document will cover all things related to poking around the app using IEX.


We can create/edit the .iex.exs file to minimize some typing when working in iex:

```shell
➜  metademo_jaxex git:(master) cat .iex.exs 
alias MetademoJaxex.{
  Repo,
  Metademo.Datacenter,
  Metademo.Host,
  Metademo.HostRole,
  Metademo.HostType,
  Metademo.Pod,
  Metademo.PodMember
}

import Ecto.Query
➜  metademo_jaxex git:(master)
```


```shell
hosts = Repo.all(Host)

datacenters = Repo.all(Datacenter)

host = Repo.get_by(Host, id: 1)
 -or-
host = Repo.get_by(Host, uuid: "ABCD-EFGH-IJKL")


host |> Repo.preload(:datacenter)
 -or
host_and_dc = host |> Repo.preload(:datacenter)
host_and_dc.datacenter


hosts = Repo.all(from h in Host, preload: :datacenter)


alias MetademoJaxex.Metademo
hosts = Metademo.list_hosts()

hosts = Repo.preload(hosts, [:datacenter, :host_role, :host_type])



# Pod Members Examples:

Repo.all(PodMember)

import Ecto.Query

q = from h in "hosts", join: pm in "pod_members", on: pm.host_id == h.id, select: [h.hostname, h.is_active]
Repo.all(q)

 
q = from pm in PodMember, join: p in Pod, on: p.id == pm.pod_id, join: h in Host, on: h.id == pm.host_id, select: {p.name, h.hostname}
Repo.all(q)


qry = from pods in Pod, join: hosts in assoc(pods, :hosts)
Repo.all(qry)

qry = from pods in Pod, join: hosts in assoc(pods, :hosts), preload: [hosts: hosts]
Repo.all(qry)

Repo.all from p in Pod, preload: [pod_members: :host, hosts: :datacenter, hosts: :host_role, hosts: :host_type]
```

