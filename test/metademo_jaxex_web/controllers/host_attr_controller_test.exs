defmodule MetademoJaxexWeb.HostAttrControllerTest do
  use MetademoJaxexWeb.ConnCase

  #  alias MetademoJaxex.Repo
  alias MetademoJaxex.Metademo
  # alias MetademoJaxex.Metademo.Host
  # alias MetademoJaxex.Metademo.HostAttr

  @create_attrs %{
    inserted_by: "MetademoJaxex Tests",
    is_active: 1,
    name: "metademo_version",
    updated_by: "MetademoJaxex Tests",
    value: "1.0.1"
  }
  # @update_attrs %{
  #   inserted_by: "MetademoJaxex Tests",
  #   is_active: 1,
  #   name: "metademo_version",
  #   updated_by: "MetademoJaxex Tests",
  #   value: "1.0.2"
  # }
  # @invalid_attrs %{inserted_by: nil, is_active: nil, name: nil, updated_by: nil, value: nil}

  #       ALOT OF WORK NEEDED HERE ***
  # @create_host %{
  #   hostname: "web.somehost01.com", host_role_id: hr_websrv.id, 
  #   host_type_id: ht_virt.id, datacenter_id: dc_austin.id, is_active: 1, 
  #   uuid: "ABCD-EFGH-IJKL", ip_address: "192.168.1.4", 
  #   inserted_by: "MetademoJaxes Tests", updated_by: "MetademoJaxes Tests"
  # }

  # @create_host_role %{
  #   name: "Web Server", description: "use for hosts that are a web server", is_active: 1, 
  #   inserted_by: "MetademoJaxex", updated_by: "MetademoJaxex"
  # }

  # @create_host_type %{
  #   name: "VIRTUAL", description: "use this type for virutal machine type hosts", is_active: 1, 
  #   inserted_by: "MetademoJaxex", updated_by: "MetademoJaxex"
  # }

  # @create_datacenter %{
  #   code: "AUS", name: "Austin", is_active: 1, pattern: ".aus.", 
  #   inserted_by: "MetademoJaxex", updated_by: "MetademoJaxex"
  # }

  # def fixture(:host_role) do
  #   {:ok, host_role} = Metademo.create_host_role(@create_host_role)
  #   host_role
  # end

  # def fixture(:host_type) do
  #   {:ok, host_type} = Metademo.create_host_type(@create_host_type)
  #   host_type
  # end

  # def fixture(:datacenter) do
  #   {:ok, datacenter} = Metademo.create_datacenter(@create_datacenter)
  #   datacenter
  # end

  def fixture(:host_attr) do
    {:ok, host_attr} = Metademo.create_host_attr(@create_attrs)
    host_attr
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all host_attrs", %{conn: conn} do
      conn = get(conn, Routes.host_attr_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  # describe "create host_attr" do
  #   test "renders host_attr when data is valid", %{conn: conn} do
  #     host = create_host

  #     conn = post(conn, Routes.host_attr_path(conn, :create), host_attr: @create_attrs)
  #     assert %{"id" => id} = json_response(conn, 201)["data"]

  #     conn = get(conn, Routes.host_attr_path(conn, :show, id))

  #     assert %{
  #              "id" => id,
  #              "inserted_by" => "MetademoJaxex Tests",
  #              "is_active" => 1,
  #              "name" => "metademo_version",
  #              "updated_by" => "MetademoJaxex Tests",
  #              "value" => "1.0.1"
  #            } = json_response(conn, 200)["data"]
  #   end

  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post(conn, Routes.host_attr_path(conn, :create), host_attr: @invalid_attrs)
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "update host_attr" do
  #   setup [:create_host_attr]

  #   test "renders host_attr when data is valid", %{conn: conn, host_attr: %HostAttr{id: id} = host_attr} do
  #     conn = put(conn, Routes.host_attr_path(conn, :update, host_attr), host_attr: @update_attrs)
  #     assert %{"id" => ^id} = json_response(conn, 200)["data"]

  #     conn = get(conn, Routes.host_attr_path(conn, :show, id))

  #     assert %{
  #              "id" => id,
  #              "inserted_by" => "MetademoJaxex Tests",
  #              "is_active" => 1,
  #              "name" => "metademo_version",
  #              "updated_by" => "MetademoJaxex Tests",
  #              "value" => "1.0.2"
  #            } = json_response(conn, 200)["data"]
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, host_attr: host_attr} do
  #     conn = put(conn, Routes.host_attr_path(conn, :update, host_attr), host_attr: @invalid_attrs)
  #     assert json_response(conn, 422)["errors"] != %{}
  #   end
  # end

  # describe "delete host_attr" do
  #   setup [:create_host_attr]

  #   test "deletes chosen host_attr", %{conn: conn, host_attr: host_attr} do
  #     conn = delete(conn, Routes.host_attr_path(conn, :delete, host_attr))
  #     assert response(conn, 204)

  #     assert_error_sent 404, fn ->
  #       get(conn, Routes.host_attr_path(conn, :show, host_attr))
  #     end
  #   end
  # end

  # defp create_host(_) do
  #  host_role  = fixture(:host_role)
  #  host_type  = fixture(:host_type)
  #  datacenter = fixture(:datacenter)

  #  host = Repo.insert!(%Host{hostname: "web.somehost01.com", host_role_id: host_role.id, 
  #    host_type_id: host_type.id, datacenter_id: datacenter.id, is_active: 1, 
  #    uuid: "ABCD-EFGH-IJKL", ip_address: "192.168.1.4", 
  #    inserted_by: "MetademoJaxex Tests", updated_by: "MetademoJaxex Tests"})
  # end

  # defp create_host_attr(_) do
  #  #host_attr = fixture(:host_attr)
  #  host_attr = Ecto.build_assoc(host, :host_attrs, @create_attrs) 
  #  #host_attr = Ecto.build_assoc(host, :host_attrs, %{name: "metademo_version", value: "1.1.1.", inserted_by: "MetademoJaxex Tests", updated_by: "MetademoJaxex Tests"}) 
  #  {:ok, host_attr: host_attr}
  # end
end
